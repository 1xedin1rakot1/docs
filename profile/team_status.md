# Командные статусы

![Командные статусы](../img/team_status_user.png)

* Раздел профиля пользователя содержащий список команд и статус установленный для пользователя в каждой из них.
* Более подробно о командном статусе рассказано в материале "[Статусы пользователей](../teams/user_status.md)", раздела "Команды".