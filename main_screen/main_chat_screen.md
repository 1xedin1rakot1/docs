# Активный чат

- Активный чат можно разделить на 3 части:
  - Заголовок.
  - Лента постов.
  - Поле ввода и редактирования.

----
**Заголовок**

  ![Заголовок](../img/head_chat.png)

  - Заголовок содержит:
    
    - Слева:
      - Название чата.
      - Аватарку чата.
      - Статус чата (о чём подробнее в разделе с описанием настроек чата).
    - Справа:
     
      - Иконка поиска, нажатие на которую открывает модальное окно с выбором параметров поиска (подробнее в разделе описания настроек и параметров поиска).
      - Иконка инициализации звонка в чат, нажатие на которую открывает диалоговое окно с выбором вариантов звонка "**Тихий**" или "**Обычный**".
      - Иконка отображения вложений в чате. Открывает окно со списком прикреплённых файлов пользователями (подробнее в разделе о прикреплённых файлах).
      - Иконка добавления или отмены статуса "**Избранный**" для данного чата.
      - Иконка открывающее расширенное меню настроек.
        
        ![Расширенное меню](../img/settings_chat.png)



----

**Лента постов**

![Пост в ленте](../img/post_feed.png)

- Лента постов содержит:
 
  - Аватарку пользователя разместившего пост, а так же индикатор статуса состояния аккаунта пользователя в данный момент в виде цветного кружка меняющего цвет в зависимости от текущего статуса. (Онлайн, активный разговор и т.п.).
  - Имя пользователя разместившего пост.
  - Дату и время опубликования поста.
  - Статус поста "Кто читал" (подробнее в соответствующем разделе).
  - Текст поста.

**Поле ввода и редактирования**

![Поле ввода](../img/text_block.png)

- Поле ввода содержит:
 
  - Слева:
   
    - Значок перемещения ленты к последнему сообщению "**Вниз**"
    Этот значок становится активным если пользователь не видит на экране последнее сообщение.
    - Значок скрепки "**Отправить файлы**", нажатие на который открывает модальное окно позволяющее прикреплять файлы к сообщению из разных источников (подробнее в разделе про файлы в чате).
    - Текст в поле ввода с подсказкой.

  - Справа:
   
    - Значок замка "**Отправить зашифрованное сообщение**", нажатие на который открывает модальное окно с настройками отправки зашифрованного сообщения (подробнее в разделе "**Шифрование**").
    - Значок смайлика, нажатие на который автоматически добавляет в поле ввода эмодзи и открывает список имеющихся эмодзи позволяя добавить в сообщение любые эмодзи оттуда.
    - Значок вопроса, нажатие на который аналогично /help, и  открывает модальное окно "**Справка по вводу и форматированию текста**" (подробнее в разделе "**Справка**")
    - Значок "**Enter**" нажатие на который дублирует нажатие на клавишу "**Enter**" на клавиатуре, и создаёт новый пост отправляя набранный в поле ввода текст и прикреплённые вложения в ленту чата.
