# Сведения

![Сведения(верх)](../../img/info_header.png)

Первая вкладка окна настройки чата. 

* Её можно условно разделить на две части. На верхней части пользователь видит  название чата, его аватарку, ссылку на файлы размещённые в чате, количество пользователей в чате, сколько из них онлайн, а так же количество групп.

* Так же, если пользователь обладает правами администратора, он видит упоминание о наличии таких прав. Если их нет, то пользователь видит только предупреждение о различных особенностях чата, видимости чата в команде,временном хранении постов и т.п.

  ![Временное хранение постов](../../img/temporary_posts.png)

* К верхней части так же можно отнести ссылки на приглашения друзей и добавления участников, которые доступны только администраторам чата и ведут на соответствующие вкладки его настроек.

---

Ко второй, нижней части вкладки "**Сведения**", можно отнести таблицу с данными пользователей чата, строку фильтрации пользователей и вкладки переключающие содержание чата.

![Нижняя часть сведений чата](../../img/down_part_info_chat.png)

* "**Людей**" - первый раздел сведений чата, показывающий как общее количество людей в чате, так и подробную таблицу данных их аккаунтов. Таблица содержит данные о статусе аккаунта в чате, наличии прав администратора и возможность для администратора удалить любого пользователя из этого чата.

  ![Людей](../../img/people_chat.png)

* "**Групп**" - второй раздел сведений в настройках чата показывающий общее количество групп находящихся в этом чате. 

  ![Групп](../../img/group_settings_info_chat.png)

  При переходе в раздел, открывается список групп, их название и количество участников в каждой.

* "**Прикреплено**" - третий раздел сведений в настройках чата показывающий количество и содержание прикреплённых сообщений в чате, если таковые есть.

  ![Закреплённые сообщения](../../img/pined_posts.png)

  ![Закреплённые сообщения 2](../../img/pined_posts2.png)

  Только администратор чата может прикреплять и откреплять сообщения. Если нажать на кнопку "**Контекст**", то откроется окно с закреплённым сообщением в контексте ленты постов, где нужный пост будет подсвечен зелёным.

* "**Задачи**" - заключительный раздел сведений в настройках чата показывающий общее количество задач поставленных в этом чате, сколько из них открыто, завершено или закрыто. 

  ![Задачи](../../img/task_info_chat.png) 

Для того что бы наделить пост статусом "**Задача**", нужно в начале поста написать "**#task**". Это превратит пост в задачу и отобразит её в этом разделе. Функционал доступен только для командных чатов. 
Чтоб завершить задачу нужно в ответе на пост с задачей написать "**#done**", а чтоб закрыть, "**#close**"












